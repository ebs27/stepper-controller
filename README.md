# stepper-controller



## About
I only used this VI the semester when the ME 344 labs went online to control a stepper motor but thought it might be useful to include anyways. 

This VI was used to control a stepper motor using LabVIEW, an NI DAQ, and a Sparkfun Easy Driver.

It uses a few digital IO and a counter to drive the motor. The channels used are DO6, DO7, PFI1, and PFI13, but you can change that in the block diagram. Need one available counter. There is a fast jog and a slow jog setting. 

VI opens in LabVIEW 2018 or later
